FROM debian:sid-slim

ENV VER 0.4.0
ENV DEBEMAIL kskarthik@disroot.org
ENV DEBFULLNAME Karthik

RUN apt update && apt install build-essential wget dh-make devscripts -y && \
    mkdir /aerc && cd aerc && \
    wget https://git.sr.ht/~sircmpwn/aerc/archive/$VER.tar.gz  && tar -xvf $VER.tar.gz

#RUN mkdir -p /aerc/aerc-$VER/debian
 
#COPY control copyright rules changelog /aerc/aerc-$VER/debian/

WORKDIR /aerc/aerc-$VER

RUN dh_make -s --createorig -y && rm -rf debian/*

COPY compat control copyright rules changelog /aerc/aerc-$VER/debian/

RUN chown 755 debian/*

RUN apt install golang scdoc -y
#RUN dpkg-buildpackage
ENTRYPOINT /bin/bash
