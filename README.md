### Building aerc email client deb file using CI/CD

[![pipeline status](https://gitlab.com/kskarthik/aerc-deb/badges/master/pipeline.svg)](https://gitlab.com/kskarthik/aerc-deb/-/commits/master) [![coverage report](https://gitlab.com/kskarthik/aerc-deb/badges/master/coverage.svg)](https://gitlab.com/kskarthik/aerc-deb/-/commits/master)

- You can download the deb file from releases section
- Works on all debian based distributions sucha as ubuntu/mint etc...
- Currenly supported architecture is `x86_64` (64 bit) only
