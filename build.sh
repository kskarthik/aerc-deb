#!/bin/bash
apt update && apt install debhelper dpkg-dev wget golang scdoc -y &&
wget https://git.sr.ht/~sircmpwn/aerc/archive/$VER.tar.gz  && tar -xvf $VER.tar.gz 
mkdir -p aerc-$VER/debian
cp -r compat changelog control copyright rules aerc-$VER/debian/
cd aerc-$VER
dpkg-buildpackage
cd ..
mkdir .public 
mv *.deb .public/
mv .public public
